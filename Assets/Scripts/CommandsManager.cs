using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class CommandsManager : MonoBehaviour
{
    public GameObject commandsInputHUD;
    public MonoBehaviour[] attachedScripts;
    public CommandsSheet commandsSheet;
    public TMP_InputField commandsInputIF;

    public GameObject invalidCommandMessageHUD;
    public float messageDuration;

    public Rigidbody rb;
    public ControlJugador cj;

    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {




        if (Input.GetKeyDown(KeyCode.F1))
        {
            // Verifica el estado actual del cursor y cambia su estado a la opci�n opuesta
            if (Cursor.lockState == CursorLockMode.Locked)
            {
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
            }

            // Muestra o oculta la consola de comandos y deshabilita o habilita los scripts de la lista "attachedScripts"
            commandsInputHUD.SetActive(!commandsInputHUD.activeSelf);
            foreach (MonoBehaviour scr in attachedScripts)
            {
                scr.enabled = !scr.enabled;
            }

        }
        else if (Input.GetKeyDown(KeyCode.Return) && commandsInputHUD.activeSelf)
        {
            TryToExecuteCommand();
        }
    }

    void TryToExecuteCommand()
    {

        if (commandsInputIF.text != "")
        {
            foreach (Command cmd in commandsSheet.commandsList)
            {
                if (commandsInputIF.text == cmd.commandname)
                {
                    Invoke(cmd.functionName, 0);
                    Debug.Log("comando ejecutado");
                    commandsInputIF.text = "";
                    commandsInputHUD.SetActive(false);

                    // Vuelve a habilitar los scripts de la lista "attachedScripts"
                    foreach (MonoBehaviour scr in attachedScripts)
                    {
                        scr.enabled = true;
                    }
                    return;
                }
            }

            commandsInputIF.text = "";
            commandsInputHUD.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            StopCoroutine(InvalidCommandToast());
            StartCoroutine(InvalidCommandToast());
            foreach (MonoBehaviour scr in attachedScripts)
            {
                scr.enabled = true;
            }

        }
    }
    void CommandGravityOff()
    {
        Physics.gravity = new Vector3(0, 0, 0);
    }
    void CommandGravityOn()
    {
        Physics.gravity = new Vector3(0, -9.81f, 0);
    }
    void CommandNoClipOn()
    {
        rb.isKinematic = true;

    }
    void CommandBiggerJump()
    {
        cj.magnitudSalto = 50f;
    }
    void CommandNormalJump()
    {
        cj.magnitudSalto = 10f;
    }
    void CommandChangeVelocityTrue()
    {
        cj.rapidezDesplazamiento = 50f;
    }
    void CommandChangeVelocityFalse()
    {
        cj.rapidezDesplazamiento = 5f;
    }
    void CommandChangeHp()
    {
        cj.hpJugador += 100;
    }
    void CommandKill()
    {
        cj.hpJugador = 0;
    }

    

    IEnumerator InvalidCommandToast()
    {
        invalidCommandMessageHUD.SetActive(true);
        invalidCommandMessageHUD.transform.localScale = Vector3.one;
        yield return new WaitForSeconds(messageDuration);
        for (float i = 1; i > 0; i -= 0.1f)
        {
            invalidCommandMessageHUD.transform.localScale = new Vector3(i, i, i);
            yield return null;
        }

        invalidCommandMessageHUD.transform.localScale = Vector3.zero;
        invalidCommandMessageHUD.SetActive(false);

    }
}
