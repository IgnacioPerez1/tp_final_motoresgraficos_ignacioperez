using System.Diagnostics;
using Debug = UnityEngine.Debug;
using UnityEngine.SceneManagement; //libreria para poder reiniciar la escena
using UnityEngine;
using System.ComponentModel;
using System.Threading;
using System.Collections.Specialized;
using System.Security.Cryptography;

public class ControlJugador : MonoBehaviour

{
    public Camera camaraPrimeraPersona;
    public float rapidezDesplazamiento = 10.0f;
    public GameObject proyectil;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    private bool dobleSalto;
    private Rigidbody rb;
    public int aceleracionDeslizamiento = 1;
    private bool puedeDeslizarse = true;
    private bool puedeDobleSalto = false;
    public int hpJugador;
    public TMPro.TMP_Text textoVida;
    public TMPro.TMP_Text textoPerdiste;
    public TMPro.TMP_Text textoGanaste;
    public TMPro.TMP_Text textoBalas;

    private int balas = 0;


    private string posicionPersonaje = "parado";
    private int saltoNormal = 1;
    private int saltoDoble = 0;
    private int cantidadSaltos = 0;
    private bool puedeSaltarDosVeces = false;
    private bool ganar = false;


    // Vector3 Jugador = new Vector(Jugador.Position.X, Jugador.Position.Y, Jugador.Position.Z);





    void Start()
    {
        hpJugador = 100;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        Cursor.lockState = CursorLockMode.Locked;
        balas = 10;
        
    }

    void FixedUpdate()
    {

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;



        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;


        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
    }

    void Update()
    {

        Debug.Log(balas);

        setearTextos();



        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }


        if (Input.GetKeyDown("z"))
        {
            ocultarYmostrarCursor();
        }


        if (balas > 0)
        {


            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                RaycastHit hit;
                GestorDeAudio.instancia.ReproducirSonido("disparo");
                balas -= 1;

                if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
                {


                    if (hit.collider.name.Substring(0, 3) == "Bot")
                    {
                        GameObject objetoTocado = GameObject.Find(hit.transform.name);
                        ControlBot scriptObjetoTocado = (ControlBot)objetoTocado.GetComponent(typeof(ControlBot));

                        if (scriptObjetoTocado != null)
                        {
                            scriptObjetoTocado.recibirDaņo();

                        }
                    }
                }
            }
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

                GameObject pro;
                pro = Instantiate(proyectil, ray.origin, transform.rotation);

                Rigidbody rb = pro.GetComponent<Rigidbody>();
                rb.AddForce(camaraPrimeraPersona.transform.forward * 100, ForceMode.Impulse);

                Destroy(pro, 1);
            }
        }
        

        // salto();

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (EstaEnPiso() && puedeDeslizarse == true)
            {
                GestorDeAudio.instancia.ReproducirSonido("deslizar");
                posicionPersonaje = "agachado";
                rb.velocity = Vector3.zero;                                                                     // para que no se pueda ir a mas velocidad de la preestablecida
                rb.AddForce(transform.forward * aceleracionDeslizamiento, ForceMode.Impulse);
                transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                puedeDeslizarse = false;
                
                Invoke("enfriamientoDeslizarse", 1.5f);
            }
        }
        if (posicionPersonaje == "agachado")
        {
            dobleSalto2();
        }
        if (posicionPersonaje == "parado")
        {
            salto();
        }



       if (EstaEnPiso())
        {
            dobleSalto = true;
        }

     
        


        if (Input.GetKey(KeyCode.F2)) { SceneManager.LoadScene(1); Time.timeScale = 1; } // si se toca la R se reinicia la escena
    }

        private bool EstaEnPiso()
        {
            return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
            col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
        

        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("coleccionable") == true)   // cuando el jugador toca un "coleccionable" este se desactiva
            {
                ;
            }
            if (other.gameObject.CompareTag("MapaFin") == true)
            {
                SceneManager.LoadScene(1);

            }
        }
        void enfriamientoDeslizarse() //una vez pasado x tiempo se desliza
        {
            puedeDeslizarse = true;
        }

    void salto()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (EstaEnPiso())
            {
                
                transform.localScale = new Vector3(1f, 1f, 1f);
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
                cantidadSaltos -= 1;
                
            }
        }
    }

    void dobleSalto2()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (EstaEnPiso())
            {
                transform.localScale = new Vector3(1f, 1f, 1f);
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            }
            else if (dobleSalto)
            {
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
              //  rb.velocity = Vector3.zero;
                dobleSalto = false;
                posicionPersonaje = "parado";
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("enemigo"))
        {
            hpJugador -= 25;

        }
        if (collision.gameObject.CompareTag("plataformaGanar"))
        {
            textoGanaste.text = "Felicitaciones Ganaste!!";
        }
        if (collision.gameObject.CompareTag("cargadores"))
        {
            balas = 10;
            Destroy(collision.gameObject);

        }
    }
    public void setearTextos()
    {
        textoVida.text = "Cantidad de Vida: " + hpJugador.ToString();
        if(hpJugador <= 0)
        {
            textoPerdiste.text = "PERDISTE";
        }
        textoBalas.text = "Balas : " + balas.ToString();
    }

    public void ocultarYmostrarCursor()
    {
        if (Cursor.lockState == CursorLockMode.Locked)
        {
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

}


   


