using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Commands Sheet" , menuName = "Commands System/Commands Sheet", order = 1000)]

public class CommandsSheet : ScriptableObject
{
    public List<Command> commandsList = new List<Command>();

}

[System.Serializable]
public class Command
{
    public string commandname;
    public string functionName;
}
